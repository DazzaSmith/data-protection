
GDPR work overview and approach
-------------------------------

This page is an overview of all the work I've done to ensure that we are in line
with GDPR/data protection laws.


The deliverables I'm including are:

* `01 Decisions and actions document <01%20Decisions%20and%20actions%20document.rst>`_

  A document detailing my recommendations about our data protection policies,
  including actions already taken, along with my reasons for everything. It
  should be read by the committee, and kept for later reference (especially as
  evidence of our due diligence regarding GDPR), but doesn't need to be read by
  leaders or become part of our manuals.

  It's a long document, but I've tried to keep it fairly readable.

* `02 Amendments to CCiW handbook <02%20Amendments%20to%20manual.rst>`_

  The main changes are:

  * additions to leader responsibilities
  * a short section on data protection which references other parts and docs.

* 03 CCiW website security docs

  The link is:
  https://gitlab.com/cciw/cciw.co.uk/-/blob/master/docs/security.rst

  This is part of the CCIW webmaster's manual, and includes information about
  security, privacy, data protection and data retention. As a lot of the GDPR
  responsibilities fall on the web master, this contains the longest section
  explicitly on data protection principles.

  Note:

  * it is part of the source code/documentation that the web developers read
  * it is therefore public, as all our source code is.
  * the audience is the web master/developers.

  I will include a copy of the most recent version when submitting this
  work to the committee.

  TODO add a significant data protection section to this.

* 04 CCiW Website access information

  The link is:
  https://docs.google.com/document/d/1_Ox8wWfqA73vqN66yEQRSASuKxtrFkcftJa5oFytiPc/edit

  This document is distinct from the previous one to allow it to include private
  information that shouldn't be a part of our public source code.

  It should be visible only to committee members, let me know if you can't access it.
  A copy of the most recent version is included. The document has been around a while but
  has been updated and completed as part of this work.

  I will include a copy of the most recent version when submitting this
  work to the committee.

* 05 Data retention policy

  TODO

* 06 Appropriate Policy Document

* 07 Privacy notice

  https://ico.org.uk/for-organisations/make-your-own-privacy-notice/
  
  TODO
