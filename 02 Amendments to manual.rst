Amendments to CCiW manual
=========================

We need a short section that notes CCiW's responsibilities to handle data
carefully, with a very brief summary of the main points. Very rough first draft
as follows:


Data protection
~~~~~~~~~~~~~~~

As an organisation it is important to respect the data protection rights of
everyone involved in camp, including and officers. This is part of our duty of
love to our neighbours, and we have specific legal obligations too, as set out
in the GDPR and other data protection laws, which we must abide by.

In summary, in terms of the application of these laws to our activities, the
main responsibilities we need to be aware of are as follows:

* We should collect only the personal data that we need to do our job, and keep
  it only as long as we need to.

* We must use this data only for the agreed purposes for which we've collected
  it.

* We should not pass the data on to third parties, or allow non-authorised
  people to access it.

* We must take particular care with certain more sensitive data, such as
  information about past criminal records that are part of the officer
  application process.

* We must report data breaches to ICO and the "data subjects" (the people the
  information is about) in certain circumstances.

The trustees have the responsibility to ensure our practices and policies are in
line with data protection law, and there are specific duties that fall to
different members of CCiW, which are detailed in other places:

* Web master/web developers: see the "Security and data protection" manual that
  is in the CCiW source code:
  https://gitlab.com/cciw/cciw.co.uk/-/blob/master/docs/security.rst

* Leaders: TODO

* Booking secretary: TODO

* DBS Officer: TODO

The trustees will be responsible for answering any queries regarding use of
personal data not covered in these manuals, and they will use resources
including the following to adjust our policies as needed in the future:

* https://gdpr-info.eu/

* https://ico.org.uk/for-organisations/in-your-sector/charity/charities-faqs/

* https://ico.org.uk/for-organisations/guide-to-data-protection/guide-to-the-general-data-protection-regulation-gdpr/
